source("my.log.R")

make.table <- function(thread.id){
    if(is.na(thread.id)) {
        return(data.frame())
    } else {
        # Create a Progress object
        progress <- shiny::Progress$new()
        progress$set(message = "Processing page(s)", value = 0)
        # Close the progress when this reactive exits (even if there's an error)
        on.exit(progress$close())
    
        updateProgress <- function(value = NULL, detail = NULL) {
            if (is.null(value)) {
                value <- progress$getValue()
                value <- value + (progress$getMax() - value) / 5
            }
            progress$set(value = value, detail = detail)
        }

        source("get.table.R")
        get.thread(thread.id, updateProgress)
    }
}

shinyServer(function(input, output) {

  datasetInput <- reactive({
      make.table(input$threadid)
  })
  
    output$table <- renderTable({
    datasetInput()
  },  include.rownames = FALSE )

  output$downloadData <- downloadHandler(
    filename = function() {
		  paste(input$threadid, input$filetype, sep = ".")
	  },
      content = function(file) {
          my.df <- datasetInput()
          sep <- switch(input$filetype, "csv" = ",", "xlsx" = "\t")
          # Write to a file specified by the 'file' argument
          if(sep == ","){
              write.csv2(my.df, file, row.names = FALSE)              
          } else {
              library("xlsx")
              write.xlsx(my.df, file, row.names=FALSE)
          }
      }
  )
})

