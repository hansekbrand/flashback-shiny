# README #

A Shiny app that can automatically download, parse, and export a discussion thread on www.flashback.org to a CSV-file. 

The intended use case is when a researcher needs to import large amounts of data from flashback to local disk for further processing in a program for qualitative analysis.

If you only want to *use* the app, you can freely use it here: http://pc245.socio.gu.se:3838/hans/fb/

The source is only needed if you want run the app on your own server.

### What is this repository for? ###

* This repository is primarily for backup purposes.

### How do I get set up? ###

* Install shiny
* Put flashback-shiny in a directory where shiny looks for apps

### Who do I talk to? ###

* Hans Ekbrand <hans.ekbrand@gmail.com>
